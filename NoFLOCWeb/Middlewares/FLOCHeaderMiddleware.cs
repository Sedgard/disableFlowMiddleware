﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NoFLOCWeb.Middlewares
{
    public class FLOCHeaderMiddleware
    {
        private readonly RequestDelegate _next;

        public FLOCHeaderMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            context.Response.OnStarting(() =>
            {
                context.Response.Headers.Add("Permissions-Policy", "interest-cohort=()");
                return Task.CompletedTask;
            });

            await _next(context);
        }
    }
}
